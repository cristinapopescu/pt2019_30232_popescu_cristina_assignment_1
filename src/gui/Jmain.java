package gui;

import javax.swing.JOptionPane;

import model.Monom;
import model.Polinom;
import servicii.Operatii;

public class Jmain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Frame frame = new Frame();
		Operatii o = new Operatii();
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		
		frame.createFrame();
		frame.frame.setVisible(true);
		
		frame.ok1.addActionListener(o1-> {
			int coef1 = Integer.parseInt(frame.coef1.getText());
			int putere1 = Integer.parseInt(frame.putere1.getText());
			
			if (putere1 < 0) {
				JOptionPane.showMessageDialog(frame, "Putere incorecta!!!");
			} else {
				Monom m = new Monom(putere1, coef1);
				p1.adaugaMonom(m);
				
				frame.polinom1.setText(p1.afisarePolinom());
				frame.coef1.setText(null);
				frame.putere1.setText(null);
				
				frame.revalidate();
				frame.repaint();		
			}
		}); 
		
		frame.ok2.addActionListener(o2-> {
			int coef2 = Integer.parseInt(frame.coef2.getText());
			int putere2 = Integer.parseInt(frame.putere2.getText());
			
			if (putere2 < 0) {
				JOptionPane.showMessageDialog(frame, "Putere incorecta!!!");
			} else {
				Monom m = new Monom(putere2, coef2);
				p2.adaugaMonom(m);
				
				frame.polinom2.setText(p2.afisarePolinom());
				frame.coef2.setText(null);
				frame.putere2.setText(null);
				
				frame.revalidate();
				frame.repaint();		
			}
		}); 
		
		frame.adunare.addActionListener(a1-> {
			frame.rezultat.setText(o.adunare(p1, p2));
		
		}); 
		
		frame.scadere.addActionListener(e-> {
			frame.rezultat.setText(o.scadere(p1, p2));
		}); 
		
		frame.inmultire.addActionListener(e-> {
			frame.rezultat.setText(o.inmultire(p1, p2));
		}); 
		
		frame.impartire.addActionListener(e-> {
			frame.rezultat.setText(o.impartire(p1, p2));
		}); 
		
		frame.derivare.addActionListener(e-> {
			frame.rezultat.setText(o.derivare(p1));
			frame.polinom2.setVisible(false);
		}); 
		
		frame.integrare.addActionListener(e-> {
			frame.rezultat.setText(o.integrare(p1));
			frame.polinom2.setVisible(false);
		}); 
		
		frame.altaOperatie.addActionListener(op-> {
			frame.rezultat.setText(null);
			o.stergere();
			frame.revalidate();
			frame.repaint();
		
		}); 
	}

}
