package gui;

import model.*;
import servicii.*;

import java.awt.*;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

	public class Frame extends JFrame {
		
		public JFrame frame = new JFrame();
		public JTextField polinom1 = new JTextField();
		public JTextField polinom2 = new JTextField();
		public JTextField rezultat = new JTextField();
		public JTextField coef1 = new JTextField();
		public JTextField coef2 = new JTextField();
		public JTextField putere1 = new JTextField();
		public JTextField putere2 = new JTextField();
		public JButton adunare = new JButton("+");
		public JButton scadere = new JButton("-");
		public JButton inmultire = new JButton("*");
		public JButton impartire = new JButton("/");
		public JButton derivare = new JButton("derivare");
		public JButton integrare = new JButton("integrare");
		public JButton ok1 = new JButton("OK");
		public JButton ok2 = new JButton("OK");
		public JButton altaOperatie = new JButton("Alta operatie");

		
		public void createFrame () {
			JPanel panel = new JPanel();
			JLabel label = new JLabel();
			JLabel label1 = new JLabel();
			JLabel egal = new JLabel();
			JLabel x = new JLabel();
			JLabel x1 = new JLabel();

			frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
			frame.setContentPane(panel);
			frame.setSize(600, 600);
			panel.setLayout(null);
			
			label.setText("Primul polinom");
			label.setSize(300, 40);
			label.setLocation(30, 20);
			panel.add(label);
			
			coef1.setSize(100, 30);
			coef1.setLocation(50, 50);
			panel.add(coef1);
			
			x.setText("* x^");
			x.setSize(50, 30);
			x.setLocation(150, 50);
			panel.add(x);
			
			putere1.setSize(100, 30);
			putere1.setLocation(180, 50);
			panel.add(putere1);
			
			ok1.setSize(70, 30);
			ok1.setLocation(280, 50);
			panel.add(ok1);
			
			polinom1.setSize(300, 30);
			polinom1.setLocation(50, 100);
			panel.add(polinom1);
			polinom1.setEnabled(false);

			
			polinom2.setSize(300, 30);
			polinom2.setLocation(50, 200);
			panel.add(polinom2);
			polinom2.setEnabled(false);
			
			label1.setText("Al doilea polinom");
			label1.setSize(300, 40);
			label1.setLocation(30, 130);
			panel.add(label1);
			
			coef2.setSize(100, 30);
			coef2.setLocation(50, 160);
			panel.add(coef2);
			
			x1.setText("* x^");
			x1.setSize(50, 30);
			x1.setLocation(150, 160);
			panel.add(x1);
			
			putere2.setSize(100, 30);
			putere2.setLocation(180, 160);
			panel.add(putere2);
			
			ok2.setSize(70, 30);
			ok2.setLocation(280, 160);
			panel.add(ok2);
			
			adunare.setSize(50, 50);
			adunare.setLocation(400, 150);
			panel.add(adunare);
			
			scadere.setSize(50, 50);
			scadere.setLocation(400, 200);
			panel.add(scadere);
			
			inmultire.setSize(50, 50);
			inmultire.setLocation(400, 250);
			panel.add(inmultire);
			
			impartire.setSize(50, 50);
			impartire.setLocation(400, 300);
			panel.add(impartire);
			
			derivare.setSize(100, 50);
			derivare.setLocation(400, 350);
			panel.add(derivare);
			
			integrare.setSize(100, 50);
			integrare.setLocation(400, 400);
			panel.add(integrare);
			
			egal.setText("=");
			egal.setSize(100, 50);
			egal.setLocation(180, 250);
			panel.add(egal);
			
			rezultat.setSize(300, 30);
			rezultat.setLocation(50, 330);
			panel.add(rezultat);
			rezultat.setEnabled(false);
			
			altaOperatie.setSize(150, 30);
			altaOperatie.setLocation(20, 400);
			panel.add(altaOperatie);
		}
		
		public JTextField getPolinom1() {
			return polinom1;
		}
	
			
		
		public void setPolinom1(JTextField polinom1) {
			this.polinom1 = polinom1;
		}

		public JTextField getPolinom2() {
			return polinom2;
		}

		public void setPolinom2(JTextField polinom2) {
			this.polinom2 = polinom2;
		}

		public JTextField getRezultat() {
			return rezultat;
		}

		public void setRezultat(JTextField rezultat) {
			this.rezultat = rezultat;
		}

		public JButton getAdunare() {
			return adunare;
		}

		public void setAdunare(JButton adunare) {
			this.adunare = adunare;
		}

		public JButton getScadere() {
			return scadere;
		}

		public void setScadere(JButton scadere) {
			this.scadere = scadere;
		}

		public JButton getInmultire() {
			return inmultire;
		}

		public void setInmultire(JButton inmultire) {
			this.inmultire = inmultire;
		}

		public JButton getImpartire() {
			return impartire;
		}

		public void setImpartire(JButton impartire) {
			this.impartire = impartire;
		}

		public JButton getDerivare() {
			return derivare;
		}

		public void setDerivare(JButton derivare) {
			this.derivare = derivare;
		}

		public JButton getIntegrare() {
			return integrare;
		}

		public void setIntegrare(JButton integrare) {
			this.integrare = integrare;
		}

		public void resetFields() {
			polinom1.setText("");
			polinom2.setText("");
		}
		
		/*public void adunare(ActionListener a1) {
			this.adunare.addActionListener(a1);
		}*/
	}


