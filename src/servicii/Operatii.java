package servicii;

import model.Monom;
import model.Polinom;
import gui.Frame;


public class Operatii {
	Polinom polinomRezultat = new Polinom();

	public String adunare (Polinom polinom1, Polinom polinom2) {
		double coeficient = 0;
		int putere = 0;
		
		boolean ok = false;
		
		for (int i = 0; i < polinom1.getPolinom().size(); i++) {
			ok = false;
			for (int j = 0; j < polinom2.getPolinom().size(); j++ ) {
				if (polinom1.getPolinom().get(i).getPutere() == polinom2.getPolinom().get(j).getPutere()) {
					coeficient = polinom1.getPolinom().get(i).getCoef() + polinom2.getPolinom().get(j).getCoef();
					putere = polinom1.getPolinom().get(i).getPutere();
					
					Monom q = new Monom(putere, coeficient);
					polinomRezultat.adaugaMonom(q);
					
					ok = true;
				}
			}
			if (ok == false) {
				coeficient = polinom1.getPolinom().get(i).getCoef();
				putere = polinom1.getPolinom().get(i).getPutere();
				
				Monom q1 = new Monom(putere, coeficient);
				polinomRezultat.adaugaMonom(q1);
			}
		}
		for (int i = 0; i < polinom2.getPolinom().size(); i++){
			ok = false;
			for (int j = 0; j < polinomRezultat.getPolinom().size(); j++) {
				if (polinom2.getPolinom().get(i).getPutere() == polinomRezultat.getPolinom().get(j).getPutere()) {
					ok = true;
				}
			}
			if (ok == false) {
				coeficient = polinom2.getPolinom().get(i).getCoef();
				putere = polinom2.getPolinom().get(i).getPutere();
				Monom a = new Monom(putere, coeficient);
				polinomRezultat.adaugaMonom(a);
			}
		}
		return polinomRezultat.afisarePolinom();
	}

	public String scadere(Polinom polinom1, Polinom polinom2) {
		double coeficient = 0;
		int putere = 0;
		
		boolean ok = false;
		
		for (int i = 0; i < polinom1.getPolinom().size(); i++) {
			ok = false;
			for (int j = 0; j < polinom2.getPolinom().size(); j++ ) {
				if (polinom1.getPolinom().get(i).getPutere() == polinom2.getPolinom().get(j).getPutere()) {
					coeficient = polinom1.getPolinom().get(i).getCoef() - polinom2.getPolinom().get(j).getCoef();
					putere = polinom1.getPolinom().get(i).getPutere();
					
					Monom q = new Monom(putere, coeficient);
					polinomRezultat.adaugaMonom(q);
					
					ok = true;
				}
			}
			if (ok == false) {
				coeficient = polinom1.getPolinom().get(i).getCoef();
				putere = polinom1.getPolinom().get(i).getPutere();
				
				Monom q1 = new Monom(putere, coeficient);
				polinomRezultat.adaugaMonom(q1);
			}
		}
		for (int i = 0; i < polinom2.getPolinom().size(); i++){
			ok = false;
			for (int j = 0; j < polinomRezultat.getPolinom().size(); j++) {
				if (polinom2.getPolinom().get(i).getPutere() == polinomRezultat.getPolinom().get(j).getPutere()) {
					ok = true;
				}
			}
			if (ok == false) {
				coeficient = polinom2.getPolinom().get(i).getCoef();
				putere = polinom2.getPolinom().get(i).getPutere();
				Monom a = new Monom(putere, coeficient);
				polinomRezultat.adaugaMonom(a);
			}
		}
		return polinomRezultat.afisarePolinom();
	}

	public String inmultire(Polinom polinom1, Polinom polinom2) {
		double coeficient = 0;
		int putere = 0;
		
		for (int i = 0; i < polinom1.getPolinom().size(); i++) {
			for (int j = 0; j < polinom2.getPolinom().size(); j++ ) {
				coeficient = polinom1.getPolinom().get(i).getCoef() * polinom2.getPolinom().get(i).getCoef();
				putere = polinom1.getPolinom().get(i).getPutere() + polinom2.getPolinom().get(i).getPutere();
				Monom b = new Monom(putere, coeficient);
				polinomRezultat.adaugaMonom(b);
			}
		}
		return polinomRezultat.afisarePolinom();
	}

	public String impartire(Polinom polinom1, Polinom polinom2) {
		double coeficient = 0;
		int putere = 0;
		Monom nou = new Monom(polinom2.getPolinom().get(0).getPutere(), polinom2.getPolinom().get(0).getCoef());
		Polinom aux = new Polinom();
		Polinom aux2 = new Polinom();
		
		for (int i = 0; i < polinom1.getPolinom().size(); i++) {
			Monom alt = new Monom(polinom1.getPolinom().get(i).getPutere(), polinom1.getPolinom().get(i).getCoef());
			aux.adaugaMonom(alt);
		}
		
		while (polinom1.getPolinom().get(0).getPutere() >= polinom2.getPolinom().get(0).getPutere()) {
			coeficient = polinom1.getPolinom().get(0).getCoef() / polinom2.getPolinom().get(0).getCoef();
			putere = polinom1.getPolinom().get(0).getPutere() - polinom2.getPolinom().get(0).getPutere();
			Monom rez = new Monom(putere, coeficient);
			polinomRezultat.adaugaMonom(rez);
			
			for (int i = 0; i < polinom2.getPolinom().size(); i++) {
				Monom z = new Monom (polinom2.getPolinom().get(i).getPutere(), polinom2.getPolinom().get(i).getCoef());
				z.coef = polinom2.getPolinom().get(i).getCoef() * rez.getCoef();
				z.putere = polinom2.getPolinom().get(i).getPutere() + rez.getPutere();
				aux2.adaugaMonom(z);				
			}
			/* SCADERE */
			coeficient = 0;
			putere = 0;
			
			boolean ok = false;
			
			for (int i = 0; i < aux.getPolinom().size(); i++) {
				ok = false;
				for (int j = 0; j < polinom2.getPolinom().size(); j++ ) {
					if (aux.getPolinom().get(i).getPutere() == polinom2.getPolinom().get(j).getPutere()) {
						coeficient = aux.getPolinom().get(i).getCoef() - polinom2.getPolinom().get(j).getCoef();
						putere = aux.getPolinom().get(i).getPutere();
						
						Monom q = new Monom(putere, coeficient);
						polinomRezultat.adaugaMonom(q);
						
						ok = true;
					}
				}
				if (ok == false) {
					coeficient = aux.getPolinom().get(i).getCoef();
					putere = aux.getPolinom().get(i).getPutere();
					
					Monom q1 = new Monom(putere, coeficient);
					aux2.adaugaMonom(q1);
				}
			}
			for (int i = 0; i < polinom2.getPolinom().size(); i++){
				ok = false;
				for (int j = 0; j < aux2.getPolinom().size(); j++) {
					if (polinom2.getPolinom().get(i).getPutere() == aux2.getPolinom().get(j).getPutere()) {
						ok = true;
					}
				}
				if (ok == false) {
					coeficient = polinom2.getPolinom().get(i).getCoef();
					putere = polinom2.getPolinom().get(i).getPutere();
					Monom a = new Monom(putere, coeficient);
					aux2.adaugaMonom(a);
				}
			}
			
		}
		return polinomRezultat.afisarePolinom();
	}

	public String derivare(Polinom polinom) {
		double coeficient = 0;
		int putere = 0;
		
		for (int i = 0; i < polinom.getPolinom().size(); i++) {
			coeficient = polinom.getPolinom().get(i).getCoef() * polinom.getPolinom().get(i).getPutere();
			putere = polinom.getPolinom().get(i).getPutere() - 1;
			Monom c = new Monom(putere, coeficient);
			polinomRezultat.adaugaMonom(c);
		}
		return polinomRezultat.afisarePolinom();
	}

	public String integrare(Polinom polinom) {
		double coeficient = 0;
		int putere = 0;
		
		for (int i = 0; i < polinom.getPolinom().size(); i++) {
			coeficient = polinom.getPolinom().get(i).getCoef() / (polinom.getPolinom().get(i).getPutere() +1);
			putere = polinom.getPolinom().get(i).getPutere() + 1;
			Monom d = new Monom(putere, coeficient);
			polinomRezultat.adaugaMonom(d);
		}
		return polinomRezultat.afisarePolinom();
	}
	
	public void stergere() {
		polinomRezultat.getPolinom().removeAll(polinomRezultat.getPolinom());
	}
}
