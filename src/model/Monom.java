package model;

public class Monom implements Comparable<Monom> {
	public int putere;
	public double coef;

	public Monom(int putere, double coef) {
		this.putere = putere;
		this.coef = coef;
	}

	public int getPutere() {
		return putere;
	}

	public void setPutere(int putere) {
		this.putere = putere;
	}

	public double getCoef() {
		return coef;
	}

	public void setCoef(double coef) {
		this.coef = coef;
	}
	
	public int compareTo (Monom m) {
		return Float.compare(m.getPutere(), this.getPutere());
	}

}
