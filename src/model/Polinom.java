package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Polinom {
	ArrayList<Monom> p;
	
	public Polinom() {
		p = new ArrayList<Monom>();
	}
	
	public  List<Monom> getPolinom() {
		return this.p;
	}
	
	public void adaugaMonom(Monom m) {
		boolean ok = false;
		
		for (int i = 0; i<this.getPolinom().size(); i++) {
			ok = false;
			if(this.getPolinom().get(i).getPutere() == m.getPutere()){
				this.getPolinom().get(i).setCoef(this.getPolinom().get(i).getCoef() + m.getCoef());
				ok = true;
			}
		}
		if (ok == false) {
			p.add(m);
			Collections.sort(p);
		}
	}
	
	public String afisarePolinom () {
		String text = "";
		
		for (int i = 0; i < this.getPolinom().size(); i++) {
			text = text + "+ (" + this.getPolinom().get(i).getCoef() + "x^" + this.getPolinom().get(i).getPutere() + ")";
		}
		
		return text;
	}
}
